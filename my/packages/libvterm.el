(use-package vterm
             :ensure t)

(use-package shell-pop
  :custom
  (shell-pop-shell-type '("vterm" "*vterm*" (lambda nil (vterm))))
  (shell-pop-full-span t)
  (shell-pop-window-position "bottom"))
 
