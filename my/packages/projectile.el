;;  projectile stuff
;; http://tuhdo.github.io/helm-projectile.html
;; set project roots
(use-package projectile
  :ensure t
  :config
  (require 'helm)
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1)
  )

(setq projectile-switch-project-action 'helm-projectile)
