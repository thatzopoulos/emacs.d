;; https://robert.kra.hn/posts/2021-02-07_rust-with-emacs/
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; rustic = basic rust-mode + additions

(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status)
              ("C-c C-c e" . lsp-rust-analyzer-expand-macro)
              ("C-c C-c d" . dap-hydra)
              ("C-c C-c h" . lsp-ui-doc-glance))
  :config
  (setq rustic-lsp-setup-p nil) ;; turns off automatic rustic lsp configuriation
  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm, but don't try to
  ;; save rust buffers that are not file visiting. Once
  ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
  ;; no longer be necessary.
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; for rust-analyzer integration

(setq lsp-rust-server 'rust-analyzer)

;; Guide on disabling/enabling lsp-mode features with pics : https://emacs-lsp.github.io/lsp-mode/tutorials/how-to-turn-off/
(use-package lsp-mode
  :ensure
  :commands lsp
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t) ;; docs that are rendered at the bottom of the window
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-completion-provider :company-mode)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

;; https://emacs-lsp.github.io/lsp-ui/
(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-position 'top) ;; options are at-point, bottom, and top
  (lsp-ui-doc-show-with-cursor t)
  (lsp-ui-doc-alignment 'window) ;; options are window or frame
  ;; remape xref-find- {definitions,references} (bound to M-. M-? by default):
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (lsp-ui-peek-enable t)
  (lsp-ui-peek-show-directory t) ;; show the directory of files
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; inline errors

(use-package flycheck :ensure)


;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; auto-completion and code snippets

(use-package yasnippet
  :ensure
  :config
  (yas-reload-all)
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))

;; Text completion framework for emacs, shows autocompletion popup as you type
(use-package company
  :ensure
  :bind
  (:map company-active-map
              ("C-n". company-select-next)
              ("C-p". company-select-previous)
              ("M-<". company-select-first)
              ("M->". company-select-last))
  (:map company-mode-map
        ("<tab>". tab-indent-or-complete)
        ("TAB". tab-indent-or-complete)))

(defun company-yasnippet-or-completion ()
  (interactive)
  (or (do-yas-expand)
      (company-complete-common)))

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "::") t nil)))))

(defun do-yas-expand ()
  (let ((yas/fallback-behavior 'return-nil))
    (yas/expand)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))
;; for Cargo.toml and other config files

(use-package toml-mode :ensure)


;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; setting up debugging support with dap-mode

(use-package exec-path-from-shell
  :ensure
  :init (exec-path-from-shell-initialize))

;; (when (executable-find "lldb-mi")
;;   (use-package dap-mode
;;     :ensure
;;     :config
;;     (dap-ui-mode)
;;     (dap-ui-controls-mode 1)
;; 
;;     (require 'dap-lldb)
;;     (require 'dap-gdb-lldb)
;;     ;; installs .extension/vscode
;;     (dap-gdb-lldb-setup)

;; Debug Templates

;; Original One I was using: 
;;     (dap-register-debug-template
;;      "Rust::LLDB Run Configuration"
;;      (list :type "lldb"
;;            :request "launch"
;;            :name "LLDB::Run"
;; 	   :gdbpath "rust-lldb"
;;            ;; uncomment if lldb-mi is not in PATH
;;            ;; :lldbmipath "path/to/lldb-mi"
;;            ))))
;;
;; One from their current documentation:
;; (dap-register-debug-template "Rust::GDB Run Configuration"
;;                              (list :type "gdb"
;;                                    :request "launch"
;;                                    :name "GDB::Run"
;;                            :gdbpath "rust-gdb"
;;                                    :target nil
;;                                    :cwd nil))


(use-package dap-mode
  :ensure
  :config
  (dap-ui-mode 1)
  ;; enables mouse hover support
  (dap-tooltip-mode 1)
  ;; use tooltips for mouse hover
  ;; if it is not enabled `dap-mode' will use the minibuffer.
  (tooltip-mode 1)
  ;; displays floating panel with debug buttons
  ;; requies emacs 26+
  (dap-ui-controls-mode 1)
  ;; (dap-auto-configure-mode)

  (require 'dap-lldb)
  (require 'dap-gdb-lldb)
  ;; installs .extension/vscode
  (dap-gdb-lldb-setup)

(dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
                           :gdbpath "rust-gdb"
                                   :target nil
                                   :cwd nil))
  )

;; (defun get-rust-binary ()
;;   "Prompt user for which binary to use and return the filename."
;;   (interactive)
;;   (let* ((bin-name (read-string "Enter the target to execute: "))
;;          (filename (concat (projectile-project-root) "target/debug/" bin-name))
;;          (does-not-exist (not (file-exists-p filename))))
;;     (when does-not-exist (message "No binary found for especified target %s!" bin-name))
;;     filename))
;; (defun get-rust-args ()
;;   "Prompt user for args to pass."
;;   (interactive)
;;   (let ((args-string (read-string "Enter the target arguments: ")))
;;      (split-string args-string)))
;; (defun dap-lldb-rust--populate-start-file-args (conf)
;;   "Populate CONF with the required arguments."
;;   (-> conf
;;       (dap--put-if-absent :dap-server-path '("/usr/bin/lldb-vscode" "1441"))
;;       (dap--put-if-absent :port 1441)
;;       (dap--put-if-absent :type "lldb")
;;       (dap--put-if-absent :cwd (car `(,(projectile-project-root))))
;;       (dap--put-if-absent :program (car `(,(get-rust-binary))))
;;       (dap--put-if-absent :name "Rust Debug")
;;       (dap--put-if-absent :program-to-start "cargo build")
;;       (dap--put-if-absent :args (car `(,(get-rust-args))))))
;; (add-hook 'rustic-mode-hook (lambda ()
;;                               (dap-register-debug-provider "lldb" 'dap-lldb-rust--populate-start-file-args)
;;                               (dap-register-debug-template "Rust Run Configuration"
;;                                                            `(:type "lldb"
;;                                                              :cwd ,(projectile-project-root)
;;                                                              :request "launch"))))
