;; Abbreviations
(setq org-link-abbrev-alist
      '(;; Abbreviations for files
        ("dev"            . "file:dev.org::*")
        ("org"            . "file:org.org::*")
        ("ref"            . "file:ref.org::*")
        ("soc"            . "file:soc.org::*")
	("prv"            . "file:private.org::*")
        ("wrk"            . "file:work.org::*")
        ;; Abbreviations for websites
        ("fedex"          . "https://www.fedex.com/fedextrack/?tracknumbers=")
        ("gmail"          . "https://mail.google.com/mail/u/0/#all/")
        ("google"         . "https://www.google.com/#q=")
        ("google-maps"    . "https://maps.google.com/maps?q=")
        ("google-scholar" . "http://scholar.google.com/scholar?hl=en&q=")
        ("github"         . "https://www.github.com/")
        ("gitlab"         . "https://www.gitlab.com/")
        ("hacker-news"    . "https://news.ycombinator.com/item?id=")
        ("instagram"      . "https://www.instagram.com/")
        ("linkedin"       . "http://www.linkedin.com/")
        ("twitter"        . "https://www.twitter.com/")
        ("ups"            . "http://www.ups.com/WebTracking/processRequest?tracknum=")
        ("usps"           . "https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels=")
        ("youtube"        . "http://www.youtube.com/user/")
        ))

;; Create abbreviations
(defun th/org-link-abbreviations-create ()
  "Replace all long form links in current file with their corresponding abbreviations in `org-link-abbrev-alist'."
  (interactive)
  (dolist (pair org-link-abbrev-alist)
    (save-excursion
      (goto-char (point-min))
      (while (search-forward (concat "[[" (cdr pair)) nil t)
        (replace-match (concat "[[" (car pair) ":"))))))

(add-hook 'org-mode-hook
          (lambda ()
            (add-to-list 'write-file-functions 'my/org-link-abbreviations-create)))

;; Remove abbreviations
(defun th/org-link-abbreviations-remove ()
  "Replace all link abbreviations in current file with their long form counterparts in `org-link-abbrev-alist'."
  (interactive)
  (dolist (pair org-link-abbrev-alist)
    (save-excursion
      (goto-char (point-min))
      (while (search-forward (concat "[[" (car pair) ":") nil t)
        (replace-match (concat "[[" (cdr pair)))))))
