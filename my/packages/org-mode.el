;;Org mode configuration
;;https://orgmode.org/worg/org-tutorials/org4beginners.html
;; Enable Org mode
(require 'org)

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c C-l") 'org-insert-link)


(use-package org
  ;;    :pin manual
  :load-path ("lisp/org-mode/lisp" "lisp/org-mode/lisp/contrib/lisp")
  :bind
  (:map org-mode-map
   ("C-c l" . org-store-link)
   ("A-h" . org-mark-element)
   ("C-a" . org-beginning-of-line)
   ("C-e" . org-end-of-line)
   ("C-k" . org-kill-line))
  :custom
  (org-directory "~/org")
  (org-log-done t)
  (org-startup-indented t)
  (org-log-into-drawer t)
  (org-special-ctrl-a/e t)
  (org-special-ctrl-k t
)
  (org-src-fontify-natively t)
  (org-src-tab-acts-natively t)
  (org-hide-emphasis-markers t)
  (org-tags-column 0)
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (latex     . t)
     (python    . t)
     (dot       . t)
     (shell     . t)
     (calc      . t)
     (sql       . t)
     (go        . t)
     (org       . t)))
     (setq org-tag-alist '(("work" . ?w) ("home" . ?h) ("emacs" . ?e) ("trivial" . ?t) ("dog" . ?d) ("purchase" . ?p) ("read" . ?r)))
     )

;; Experimental section
(setq org-attach-id-dir "~/org/data/")
(setq org-link-abbrev-alist
      '(("ggle" . "http://www.google.com/search?q=%s")
	("gmap" . "http://maps.google.com/maps?q=%s")
	))
(defun th/org-pdf-app (file-path link-without-schema)
  "Open pdf file using pdf-tools and go to the specified page."
  (let* ((page (if (not (string-match "\\.pdf::\\([0-9]+\\)\\'"
                                      link-without-schema))
                   1
                 (string-to-number (match-string 1 link-without-schema)))))
    (find-file-other-window file-path)
    (pdf-view-goto-page page)))
(setq org-file-apps
      '((auto-mode . emacs)
	(directory . emacs)
        ("\\.x?html?\\'" . default)
        ("\\.pdf\\(::[0-9]+\\)?\\'" . th/org-pdf-app)
        ("\\.mp4\\'" . "vlc \"%s\"")
        ("\\.mkv" . "vlc \"%s\"")))
;; Some org-babel stuff
;; Most of these are the default values
(setq org-babel-default-header-args
      '((:session . "none")
        (:results . "replace")
        (:exports . "code")
        (:cache . "no")
        (:noweb . "yes") ;; not the default
        (:hlines . "no")
        (:tangle . "no")
        (:padnewline . "yes")
        (:engine . "postgresql") ; Postgres-specific setting
        (:dbhost . "localhost") ; Postgres-specific setting
        (:dbuser . "postgres") ; Postgres-specific setting
	(:lang . "python") ; Python-specific setting
        (:results . "output"))) ; Python-specific setting

(setq org-edit-src-content-indentation 0) ;; default is 2
;; Stop asking me to confirm before running code in links for sh and elisp
;; Probably dangerous but I only run my own code snippets so its fine
(setq org-link-elisp-confirm-function nil)
(setq org-link-shell-confirm-function nil)


(use-package ob-go)
; Make org babel skip asking you to type yes when running code snippets
(setq org-confirm-babel-evaluate nil)

;; Make Org mode work with files ending in .org
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

;; cannot set a headline to done if children aren't done
(setq-default org-enforce-todo-dependencies t)

;; Moving a task to CANCELLED adds a CANCELLED tag
;; Moving a task to WAITING adds a WAITING tag
;; Moving a task to HOLD adds WAITING and HOLD tags
;; Moving a task to a done state removes WAITING and HOLD tags
;; Moving a task to TODO removes WAITING, CANCELLED, and HOLD tags
;; Moving a task to NEXT removes WAITING, CANCELLED, and HOLD tags
;; Moving a task to DONE removes WAITING, CANCELLED, and HOLD tags
;; tags uare used to filter taks in the agenda views 
(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))


;; Org Capture Templates For New Entries
(global-set-key (kbd "C-c c") 'org-capture)
;; Capture templates that clock in and out of tasks
(setq org-capture-templates
      (quote (("t" "todo" entry (file "~/org/inbox.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
	      ("w" "todo" entry (file "~/org/work.org")
	       "* TODO %? :WORK:\n%U\n" :clock-in t :clock-resume t)
              ("c" "Create Task For Today"
               entry
               (file+headline "~/org/gtd.org" "Scheduled Tasks")
	       "** NEXT %?\nSCHEDULED: %t\n%U\n")
("r" "read" entry (file "~/org/inbox.org")
         "* TODO %? %c :read:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("n" "note" entry (file "~/org/inbox.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("m" "Meeting" entry (file "~/org/work.org")
               "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t :jump-to-captured t)
	      )))
(add-to-list 'org-capture-templates
            '("a" "Anki basic"
               entry
               (file+headline org-my-anki-file "Dispatch Shelf")
               "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Basic\n:ANKI_DECK: Mega\n:END:\n** Front\n%?\n** Back\n%x\n"))
(add-to-list 'org-capture-templates
             '("A" "Anki cloze"
               entry
               (file+headline org-my-anki-file "Dispatch Shelf")
               "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Cloze\n:ANKI_DECK: Mega\n:END:\n** Text\n%x\n** Extra\n"))

(setq org-html-validation-link nil)
;; RETURN wil follow links in org-mode files
(setq org-return-follows-link t)
(setq org-todo-keywords
  '((sequence "TODO(t)" "NEXT(n)" "WORKING(w)" "|" "DONE(d!)" "CANCELLED(c)")
    (sequence "|" "HOLD(h)" "MEETING(m)")))

(setq org-todo-keyword-faces
       '(("TODO"    . "green")
 	("CANCELLED". "grey")
 	("WORKING" . "yellow")
 	("HOLD"    . "red")
 	("DONE"    . "grey")))

;; Refiling
; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 3))))
(setq org-refile-use-outline-path 'file)
(setq org-outline-path-complete-in-steps nil)


;; Agenda Stuff

;; org super agenda
(use-package org-super-agenda)

;; Don't show future versions of repeating tasks
(setq org-agenda-show-future-repeats nil)

;; Start week on Sunday instead of monday
(setq org-agenda-start-on-weekday 0)

(setq org-agenda-custom-commands
	  '(("p" "Personal"
	 ((agenda ""
		  ((org-agenda-span
		    (quote day))
			 (org-agenda-files (
quote ("~/org/gtd.org" "~/org/personal.org" "~/org/projects.org")))
		   (org-deadline-warning-days 7)
		   (org-agenda-overriding-header "Agenda\n")))
	  (todo "TODO"
		((org-agenda-overriding-header "To Refile\n")
				      (org-agenda-prefix-format "  ")
		 (org-agenda-files
		  (quote
		   ("~/org/inbox.org")))))
	  (todo "NEXT"
		((org-agenda-overriding-header "Projects\n")
		 ;; %c the category of the item, "Diary" for entries from the diary,
                 ;; or as given by the CATEGORY keyword or derived from the file name
                 ;; %e the effort required by the item
                 ;; %l the level of the item (insert X space(s) if item is of level X)
                 ;; %i the icon category of the item, see `org-agenda-category-icon-alist'
                 ;; %T the last tag of the item (ignore inherited tags, which come first)
                 ;; %t the HH:MM time-of-day specification if one applies to the entry
                 ;; %s Scheduling/Deadline information, a short string
                 ;; %b show breadcrumbs, i.e., the names of the higher levels
                 ;; %(expression) Eval EXPRESSION and replace the control string
                 ;; by the result
		       (org-agenda-prefix-format " %:c (%?e) %?-12t %s | ")
		 (org-agenda-files
		  (quote
		   ("~/org/projects.org" "~/org/gtd.org")))))
	  ))

	  ("w" "Work"
                       ((agenda ""
                   ((org-agenda-span
                     (quote day))
                          (org-agenda-files (quote ("~/org/work.org")))
                    (org-deadline-warning-days 14)
                    (org-agenda-overriding-header "Via\n")))
         (todo "TODO"
               ((org-agenda-overriding-header "To Refile\n")
                (org-agenda-files
                 (quote
                  ("~/org/inbox.org" "~/org/inbox-work.org")))))
         (todo "NEXT"
               ((org-agenda-overriding-header "Projects\n")
                (org-agenda-prefix-format "  %i %-12:c [%e] ")
                (org-agenda-files
                 (quote
                  ("~/org/work.org")))))
       (todo "HOLD"
               ((org-agenda-overriding-header "Waiting on others\n")
                (org-agenda-files
                 (quote
                  ("~/org/work.org")))))
       (todo "WORKING"
               ((org-agenda-overriding-header "Working Currently\n")
                (org-agenda-files
                 (quote
                  ("~/org/work.org" "~/org/gtd.org" "~/org/projects.org")))))
       )


		       )
	   ("e" "Experiments" tags "Type={.}"
  ((org-agenda-files
     (directory-files-recursively
        "~/org/areas/experiments" "\\.org$"))
     (org-overriding-columns-format
        "%35Item %Start %End %Status %Outcome")
     (org-agenda-cmp-user-defined
     (cmp-date-property-stamp "Start"))
     (org-agenda-sorting-strategy
        '(user-defined-down))
     (org-agenda-overriding-header "C-u r to re-run Type={.}")
     (org-agenda-mode-hook
        (lambda ()
           (visual-line-mode -1)
           (setq truncate-lines 1)
           (setq display-line-numbers-offset -1)
           (display-line-numbers-mode 1)))
     (org-agenda-view-columns-initially t)))
("u" "Super view"
           ((agenda "" ( (org-agenda-span 1)
                (org-super-agenda-groups
                '(
                 (:name "Today"
                  :tag ("birthday" "anniversary" "holidays" "calendar" "today")
                  :time-grid t
                  :todo ("WORKING")
                  :deadline today
                  :scheduled today)
               (:name "Overdue"
                  :deadline past)
               (:name "Reschedule"
                  :scheduled past)
               (:name "Personal"
                  :tag "personal")
               (:name "Due Soon"
                  :deadline future
                  :scheduled future)
                   ))))
        (tags (concat "w" (format-time-string "%V")) ((org-agenda-overriding-header  (concat "--\nToDos Week " (format-time-string "%V")))
             (org-super-agenda-groups
              '((:discard (:deadline t))
                (:discard (:scheduled t))
                (:discard (:todo ("DONE")))
                (:name "Ticklers"
                   :tag "someday")
                (:name "Personal"
                   :and (:tag "personal" :not (:tag "someday")))
                (:name "Ping"
                   :tag "crm")
                ))))
        ))
("z" "Hatz view"
         ((agenda "" ((org-agenda-span 'day)
                      (org-super-agenda-groups
                       '((:name "Today"
                          :time-grid t
                          :date today
                          :todo "TODAY"
                          :scheduled today
                          :order 1)))))
          (alltodo "" ((org-agenda-overriding-header "")
                       (org-super-agenda-groups
                        '(;; Each group has an implicit boolean OR operator between its selectors.
                          (:name "Today"
                           :deadline today
                           :face (:background "black"))
			  
                          (:name "Passed deadline"
                           :and (:deadline past :todo ("TODO" "WAITING" "HOLD" "NEXT"))
                           :face (:background "#7f1b19"))
                          (:name "Tachibana General Labs Projects"
				 :and (:tag "tachibana" :todo ("WORKING" "NEXT")))
                          (:name "Next Up"
                           :and (:todo ("NEXT")))
                          (:name "Important"
                           :priority "A")
                          (:priority<= "B"
                           ;; Show this section after "Today" and "Important", because
                           ;; their order is unspecified, defaulting to 0. Sections
                           ;; are displayed lowest-number-first.
				       :order 1)
			  (:name "Due Soon"
                                 :deadline future
                                 :order 8)
			  (:name "Emacs"
                                 :tag "Emacs"
                                 :order 13)
			  (:name "Research"
                                 :tag "Research"
                                 :order 15)
                          (:name "To read"
                                 :tag "Read"
                                 :order 30)
                          (:name "Waiting"
                           :todo "WAITING"
                           :order 9)
                          (:name "On hold"
                           :todo "HOLD"
                           :order 10)
                          (:name "trivial/batch"
                                 :priority<= "C"
                                 :tag ("Trivial" "Unimportant")
                                 :todo ("SOMEDAY" )
                                 :order 90)
			  (:name "Books/Articles/Videos to read/watch"
				 :and (:todo ("TODO" "NEXT") :tag ("read" "books" "article" "video")))
			  (:discard (:anything t)) ;; anything you do not discard will show up below in the 'Other items' section which I do not want in this view.
			  ))))))))
(add-hook 'org-agenda-mode-hook 'org-super-agenda-mode)


  ; general is needed for origami keymaps for org-super-agenda
(use-package general :ensure nil)

(use-package origami
  :hook (org-agenda-mode . origami-mode)
  :general
  (:keymaps 'org-super-agenda-header-map
                     "<tab>" #'origami-toggle-node))
;;;; Refile settings
; Exclude DONE state tasks from refile targets
(defun th/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'th/verify-refile-target)
;; set indentation
(setq org-startup-indented t)

(defun zz/add-file-keybinding (key file &optional desc)
  (lexical-let ((key key)
                (file file)
                (desc desc))
    (global-set-key (kbd key) (lambda () (interactive) (find-file file)))
    (which-key-add-key-based-replacements key (or desc file))))
; keybindings for commonly used org files
(zz/add-file-keybinding "C-c f w" "~/org/work.org" "work.org")
(zz/add-file-keybinding "C-c f i" "~/org/inbox.org" "inbox.org")
(zz/add-file-keybinding "C-c f g" "~/org/gtd.org" "gtd.org")
(zz/add-file-keybinding "C-c f p" "~/org/projects.org" "projects.org")
;; archives completed TODOs be default to the archive.org file in the same directory as the source file under the "date tree" corresponding to the task's closed date. Allows me to separate work from non-work stuff.
;; Can be overridden for specific files by specifying the desired value of org-archive-location in the #_archive: property at the top of the file
;; C-c C-x C-a (org-archive-subtree-default)
(use-package org-archive
:ensure nil
:custom
(org-archive-location "~/org/archive/%s_archive::"))


; Exporting stuff
(use-package ox-md
  :ensure nil
  :defer 3
  :after org)

(use-package ox-jira
  :defer 3
  :after org)

(use-package ox-hugo
:defer 3
:ensure t
:after org)


;; Beautify org mode

;; replace headlines with bullets
 (use-package org-bullets
    :ensure t
        :init
        (add-hook 'org-mode-hook (lambda ()
				   (org-bullets-mode 1))))


;; allows you use to <s TAB to insert code blocks
;; (require 'org-tempo)

(defun ar/org-insert-link-dwim ()
  "Like `org-insert-link' but with personal dwim preferences."
  (interactive)
  (let* ((point-in-link (org-in-regexp org-link-any-re 1))
         (clipboard-url (when (string-match-p "^http" (current-kill 0))
                          (current-kill 0)))
         (region-content (when (region-active-p)
                           (buffer-substring-no-properties (region-beginning)
                                                           (region-end)))))
    (cond ((and region-content clipboard-url (not point-in-link))
           (delete-region (region-beginning) (region-end))
           (insert (org-make-link-string clipboard-url region-content)))
          ((and clipboard-url (not point-in-link))
           (insert (org-make-link-string
                    clipboard-url
                    (read-string "title: "
                                 (with-current-buffer (url-retrieve-synchronously clipboard-url)
                                   (dom-text (car
                                              (dom-by-tag (libxml-parse-html-region
                                                           (point-min)
                                                           (point-max))
                                                          'title))))))))
          (t
           (call-interactively 'org-insert-link)))))

;; org roam
;; Dependencies: dash, f, s, org, emacsql, emacsql-sqlite, magit-section

;; Custom fn that creates a new note and inserts a link in the current doc without opening the new notes buffer
;; C-c n I is what I will probably bind it to
(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))



(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t) ;; Skips warning message about migrating to org-roam v2
  :custom
  ; file-truename is only necessary when using symbolic link to org-roam-direcdtory. Org-roam won't auto resolve the symbolic link to the  dir
  (org-roam-directory (file-truename "~/org/roam"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n r" . org-roam-node-random)	
         ("C-c n i" . org-roam-node-insert)
	 ("C-c n I" . org-roam-node-insert-immediate)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today)
         (:map org-mode-map
	             ("C-M-i"   . completion-at-point) ; Does not work yet TODO
	 )
         )
  :config
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

;; If emacs version >=29, set this to get the db to load correctly
  (use-package emacsql-sqlite-builtin)
  (setq org-roam-database-connector 'sqlite-builtin)

;; default capture template for reference
(setq org-roam-capture-templates
      '(
     ("d" "default" plain "%?"
       :if-new
       (file+head "${slug}.org"
                  "#+title: ${title}\n#+filetags: :notes:public:draft:\n#+created: %U\n#+lastmod: %U\n\n")
       :immediate-finish t
       :unnarrowed t)
        ("r" "reference" plain "%?" ;; Currently reference is just something with a bibtex citation
         :if-new
         (file+head "${slug}.org" "#+title: ${title}\n#+filetags: :reference:\n#+created: %U\n#+lastmod: %U\n\n") ;; date time string is the default
         :immediate-finish t
         :unnarrowed t)
        ("v" "private" plain "%?" ;; Stuff to not export to the digital garden site / page rank calculations / etc.
         :if-new
         (file+head "${slug}.org" "#+title: ${title}\n#+filetags: :private:draft:\n#+created: %U\n#+lastmod: %U\n\n") ;; date time string is the default
         :immediate-finish t
         :unnarrowed t)
        ("b" "blogpost" plain "%?" ;; Stuff tagged to export with org hugo as a post as opposed to a note?
         :if-new
         (file+head "${slug}.org" "#+title: ${title}\n#+filetags: :public:blogpost:draft:\n#+created: %U\n#+lastmod: %U\n\n") ;; date time string is the default
         :immediate-finish t
         :unnarrowed t)
        ("x" "x-files" plain "%?" ;; Stuff tagged to export with org hugo as a post as opposed to a note?
         :if-new
         (file+head "${slug}.org" "#+title: ${title}\n#+filetags: :xfiles:private:draft:\n#+created: %U\n#+lastmod: %U\n\n") ;; date time string is the default
         :immediate-finish t
         :unnarrowed t)
  ("e" "experiments" plain "%?"
	 :target (file+head "~/org/areas/experiments/${slug}.org" "#+TITLE: ${title}
#+CREATED: %u
#+MODIFIED:

#* ${title}
:PROPERTIES:
:Type: Experiments
:Start:
:End:
:Assess: <yyyy-mm-dd aaa>
:Questions:
:Status:
:Outcome:
:END:

* Actions
* Hypothesis
* Treatment
* Notes
* Outcome
"):unnarrowed t)
     ("i" "interview" plain "%?"
       :if-new
       (file+head "${slug}.org"
      "#+title: ${title}\n#+filetags: :notes:private:interview:draft:\n#+created: %U\n#+lastmod: %U\n\n* Job Description\n\n* Salary and Benefits Info\n\n* Remote/In-Office\n\n* Questions to Ask\n** Team Size\n** Remote / In office\n** On Call Hours\n** How long have they been working there?\n** Has the team grown in the past year?\n\n* Interview Process\n** Online reviews of the interview process\n\n* Possible Practice Problems\n\n* Notes on Interview Progress\n\n* Other Useful Information\n\n")
       :immediate-finish t
       :unnarrowed t)
	)
)

;; Update timestamp whenever file is saved, should work with Hugo templates
(setq time-stamp-active t
      time-stamp-start "#\\+lastmod:[ \t]*" ;; lastmod keeps it working with hugo templates that support lastmod
      time-stamp-end "$"
      time-stamp-format "\[%Y-%02m-%02d %3a %02H:%02M\]")
(add-hook 'before-save-hook 'time-stamp nil)
;; Change the display template, was originally "${title}", now also displays tags in the searchresults
(setq org-roam-node-display-template
      (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
(setq org-file-tags '("emacs" ""))

;; Experimental Org Roam Note Stuff Goes below:
;; This is probably going to cause problems with startup time
;; (use-package org-roam-ui
;;   :after org-roam
;; ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;; ;;         a hookable mode anymore, you're advised to pick something yourself
;; ;;         if you don't care about startup time, use the below hook:
;;   :hook (after-init . org-roam-ui-mode)
;;   :config
;;   (setq org-roam-ui-sync-theme t
;;         org-roam-ui-follow t
;;         org-roam-ui-update-on-save t
;;         org-roam-ui-open-on-start nil))

(use-package emacsql
  :ensure t
  )
(use-package f
  :ensure t
  )
(use-package s
  :ensure t
  )

(use-package emacsql-sqlite
  :ensure t
  )

(use-package deft
:config
(setq deft-directory org-directory
      deft-recursive t
      deft-strip-summary-regexp ":PROPERTIES:\n\\(.+\n\\)+:END:\n"
      deft-use-filename-as-title t)
:bind
("C-c n d" . deft))

; function that expands file name and appends it to the org-directory
(defun dw/org-path (path)
  (expand-file-name path org-directory))

;;; Org-journal
;; https://github.com/bastibe/org-journal
(use-package org-journal
  :ensure t
  :custom
  (org-journal-date-prefix "#+TITLE: ")
  (org-journal-time-prefix "* ")
  (org-journal-date-format "%a, %Y-%m-%d")
  (org-journal-file-format "%Y-%m-%d-journal.org")
  (org-journal-file-type 'daily) ;; You can change it to 'weekly if desired
  (org-journal-start-on-weekday 1) ;; 1 corresponds to Monday
  (org-journal-dir "~/org/journal/")
  :bind
  (("C-c C-j" . org-journal-new-entry)))

;; Org-ref
(use-package org-ref
  :ensure t
  )

(use-package helm-bibtex
  :ensure t
  :bind
  (("C-c n B" . helm-bibtex))
  )

;; https://github.com/org-roam/org-roam-bibtex
(use-package org-roam-bibtex
  :ensure t
  :after (org-roam helm-bibtex)
  :bind (:map org-mode-map ("C-c n b" . orb-note-actions)) ;; this should open the pdf if im in a note created by org-roam-bibtex that has aa ROAMS_REF with a valid citation
  :config
  (require 'org-ref))
  (org-roam-bibtex-mode)

(setq bibtex-completion-bibliography '("~/Dropbox/org/biblio.bib")
	bibtex-completion-pdf-field "file"
	bibtex-completion-library-path '("~/Zotero/storage/")
	bibtex-completion-notes-path "~/Zotero/storage/"
	bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"

	bibtex-completion-additional-search-fields '(keywords abstract) ;; allows you to search for keywords and abstract
	bibtex-completion-display-formats
	'((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
	  (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
	  (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
	bibtex-completion-pdf-open-function
	(lambda (fpath)
	  (call-process "chromium" nil 0 nil fpath)) ;; was open instead of chromium initially
      )

(setq org-latex-pdf-process
      '("pdflatex -interaction nonstopmode -output-directory %o %f"
	"bibtex %b"
	"pdflatex -interaction nonstopmode -output-directory %o %f"
	"pdflatex -interaction nonstopmode -output-directory %o %f"))


;; org-clock stuff
;; Resume clocking task when emacs is restarted
(org-clock-persistence-insinuate)
;; Show lot of clocking history so it's easy to pick items off the C-F11 list
(setq org-clock-history-length 23)
;; Resume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)
;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)
;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
(setq org-clock-persist t)
;; Include current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)



(defun cmp-date-property-stamp (prop)
	"Compare two `org-mode' agenda entries, `A' and `B', by some date property.
If a is before b, return -1. If a is after b, return 1. If they
are equal return nil."
	(lexical-let ((prop prop))
	 #'(lambda (a b)

		(let* ((a-pos (get-text-property 0 'org-marker a))
					 (b-pos (get-text-property 0 'org-marker b))
					 (a-date (or (org-entry-get a-pos prop)
											 (format "<%s>" (org-read-date t nil "now"))))
					 (b-date (or (org-entry-get b-pos prop)
											 (format "<%s>" (org-read-date t nil "now"))))
					 (cmp (compare-strings a-date nil nil b-date nil nil))
					 )
			(if (eq cmp t) nil (signum cmp))
			))))
