;; Go - lsp-mode
;; Make sure gopls is installed: GO111MODULE=on go get golang.org/x/tools/gopls@latest

;; go mode is required for highlighting, indentation, etc.
(use-package go-mode :ensure)

;; Company mode
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

;; Set up before-save hooks to format buffer and add/delete imports.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)
