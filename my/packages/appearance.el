;; Disable the splash screen (to enable it agin, replace the t with 0)
(setq inhibit-startup-message t)

;; Remove scroll bar, menu bar and tool bar
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)

;; Turn off that annoying bell
(setq ring-bell-function 'ignore)

;; Make fonts prettier
(set-face-attribute 'default nil :font "Iosevka" :height 130)

;; Enable transient mark mode
(transient-mark-mode 1)

;; Show trailing spaces
;; (setq-default show-trailing-whitespace t)

;; theme
(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-one t))


;; All The Icons
(use-package all-the-icons)