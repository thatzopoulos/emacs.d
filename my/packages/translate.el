;; Google translate : https://github.com/atykhonov/google-translate
(use-package google-translate)

;; I don't seem to be using this one?
(defun th/google-translate--search-tkk ()
  "Search TKK."
  (list 430675 2721866130))

(defun th/google-translate-word-at-point ()
  (interactive)
  (let ((w (thing-at-point 'word)))
    (google-translate-translate "auto" "en" w)))

(global-set-key (kbd "C-c t") (lambda (s) (interactive "sTranslate: ")
				(google-translate-translate "auto" "en" s)))
(global-set-key (kbd "C-c T") 'google-translate-word-at-point)
