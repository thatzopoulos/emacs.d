(require 'package)

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")  ("nongnu" . "https://elpa.nongnu.org/nongnu/")
 ("melpa" . "http://melpa.org/packages/") ))

(setq package-enable-at-startup nil)
(package-initialize)


; use-package nonsense
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

; indicates use-package should always try to install missing packages
(custom-set-variables '(use-package-always-ensure t))


(use-package bind-key)
(require 'bind-key)

;; Which Key
(use-package which-key
  :ensure t
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  :config
  (which-key-mode))

;; Powerline
(use-package spaceline
  :ensure t
  :init
  (setq powerline-default-separator 'slant)
  :config
  (spaceline-emacs-theme)
  (spaceline-toggle-minor-modes-off)
  (spaceline-toggle-buffer-size-off)
  (spaceline-toggle-evil-state-on))
