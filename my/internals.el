;; Set System Font
(when (member "Iosevka" (font-family-list))
  (set-fontset-font "fontset-default" nil
                    (font-spec :size 20 :name "Iosevka")))

(when (member "Symbola" (font-family-list))
  (set-fontset-font t 'unicode "Symbola" nil 'prepend))

;; Set utf-8 encoding everywhere explicitly
(set-language-environment "UTF-8")
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; reports how long and how many garbage collections the startup took, runs at the very end due to the hook used so that it does not get clobbered by other messages during startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))


;; bookmarks
(define-key global-map [f9] 'bookmark-set)
(define-key global-map [f11] 'bookmark-jump)
;;define file to use.
(setq bookmark-default-file "~/.emacs.d/bookmarks")
(setq bookmark-save-flag 1)

; auto reload files that have changed on disk
(global-auto-revert-mode 1)

;; Show matching parens
(show-paren-mode 1)
(setq show-paren-style 'parenthesis)

;; Store all backup and autosave files in ~/.emacs.d/backups
(custom-set-variables
 '(backup-directory-alist
   `(("." . ,(concat user-emacs-directory "backups")))))

;; disable #. files
(setq create-lockfiles nil)

;; Add local bins to exec-path
(add-to-list 'exec-path "/usr/local/bin")
;; so that mac can find cmake?
(add-to-list 'exec-path "/opt/homebrew/bin")


;; Keep customization mess out of init.el
(setq custom-file (concat user-emacs-directory "_customize.el"))
(load custom-file t)

; cl library to enable additional macros (currently only used for lexical-let)
(require 'cl)


;; Browser Settings
;; Toggles if urls are opened with default macosx browser or chromium. This allows me to launmch Chrome.app while on osx
(defun th/browse-url-toggle ()
    (interactive)
      (if (eq browse-url-browser-function 'browse-url-generic)
              (progn (setq browse-url-browser-function 'browse-url-default-macosx-browser)
                           (message "Browser set to default macosx browser"))
                  (setq browse-url-browser-function 'browse-url-generic)
                      (message "Browser set to generic")))

(defun th/open-in-firefox (url &optional new-window)
  "Open URL in Firefox on macOS."
  (interactive (browse-url-interactive-arg "URL: "))
  (start-process "firefox" nil "open" "-a" "Firefox" url))

(defun th/select-browser ()
  (interactive)
  (let* ((browsers '(("Firefox" . th/open-in-firefox)
                     ("Chromium" . browse-url-chromium)
                     ("Chrome" . browse-url-chrome)))
         (choice (completing-read "Select browser: " (mapcar 'car browsers) nil t)))
    (setq browse-url-browser-function (cdr (assoc choice browsers)))
    (message "Browser set to %s" choice)))

;; Default setting (optional, only if you want to have a default)
(setq browse-url-browser-function 'browse-url-generic)
(setq browse-url-generic-program "chromium")

;; Dired Stuff

;; updates-find-ls-option to use human readable file sizes instead of the default
;; used by find-name-dired to turn a glob-style query into a dired buffer. 
(require `find-dired)
(setq find-ls-option '("-exec ls -ldh {} +" . "-ldh"))

;; dired-x ships with Emacs but isn't enabled by default, should allow me to open pdfs, unzip files, etc based on their extension as a default action
(require 'dired-x)
  ;; (define-key dired-mode-map "\C-cd" 'dired-clean-tex)
  (setq dired-guess-shell-alist-user
	(list
	 (list "\\.pdf$" "evince &")
	 (list "\\.docx?$" "libreoffice &")
	 (list "\\.aup?$" "audacity")
	 (list "\\.pptx?$" "libreoffice &")
	 (list "\\.odf$" "libreoffice &")
	 (list "\\.odt$" "libreoffice &")
	 (list "\\.odt$" "libreoffice &")
	 (list "\\.kdenlive$" "kdenlive")
	 (list "\\.svg$" "gimp")
	 (list "\\.csv$" "libreoffice &")
	 (list "\\.sla$" "scribus")
	 (list "\\.od[sgpt]$" "libreoffice &")
	 (list "\\.xls$" "libreoffice &")
	 (list "\\.xlsx$" "libreoffice &")
	 (list "\\.txt$" "gedit")
	 (list "\\.sql$" "gedit")
	 (list "\\.css$" "gedit")
	 (list "\\.jpe?g$" "sxiv")
	 (list "\\.png$" "sxiv")
	 (list "\\.gif$" "sxiv")
	 (list "\\.psd$" "gimp")
	 (list "\\.xcf" "gimp")
	 (list "\\.xo$" "unzip")
	 (list "\\.3gp$" "vlc")
	 (list "\\.mp3$" "vlc")
	 (list "\\.flac$" "vlc")
	 (list "\\.avi$" "vlc")
	 ;; (list "\\.og[av]$" "vlc")
	 (list "\\.wm[va]$" "vlc")
	 (list "\\.flv$" "vlc")
	 (list "\\.mov$" "vlc")
	 (list "\\.divx$" "vlc")
	 (list "\\.mp4$" "vlc")
	 (list "\\.webm$" "vlc")
	 (list "\\.mkv$" "vlc")
	 (list "\\.mpe?g$" "vlc")
	 (list "\\.m4[av]$" "vlc")
	 (list "\\.mp2$" "vlc")
	 (list "\\.pp[st]$" "libreoffice &")
	 (list "\\.ogg$" "vlc")
	 (list "\\.ogv$" "vlc")
	 (list "\\.rtf$" "libreoffice &")
	 (list "\\.ps$" "gv")
	 (list "\\.mp3$" "play")
	 (list "\\.wav$" "vlc")
	 (list "\\.rar$" "unrar x")
	 ))
  (setq dired-tex-unclean-extensions
	'(".toc" ".log" ".aux" ".dvi" ".out" ".nav" ".snm"))

(setq list-directory-verbose-switches "-al")
(setq dired-listing-switches "-l")
(setq dired-dwim-target t)
(setq dired-maybe-use-globstar t)
(setq dired-omit-mode nil)
;(setq dired-recursive-copies 'always)
;(setq dired-recursive-deletes 'always)
(setq delete-old-versions t)
