;; Thomas Hatzopoulos

;; Now this is podracing

;; optionally sets debug-on-error for when I'm trying to figure out what I broke in my config
(setq debug-on-error t)

;; "Set up Emacs' `exec-path' and PATH environment variable to match
;; that used by the user's shell.
;; https://www.emacswiki.org/emacs/ExecPath
;; (defun set-exec-path-from-shell-PATH ()
;;   (interactive)
;;   (let ((path-from-shell (replace-regexp-in-string
;; 			  "[ \t\n]*$" "" (shell-command-to-string
;; 					  "$SHELL --login -c 'echo $PATH'"
;; 						    ))))
;;     (setenv "PATH" path-from-shell)
;;     (setq exec-path (split-string path-from-shell path-separator))))
;; (set-exec-path-from-shell-PATH)


;; Fixing issues with PATH not appearing when Emacs is launched as an App on OSX
(use-package exec-path-from-shell
  :ensure t
  :init
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)
    (exec-path-from-shell-copy-env "GCAL_CLIENT_ID")
    (exec-path-from-shell-copy-env "GPG_KEY_ID")
    (exec-path-from-shell-copy-env "GCAL_CLIENT_SECRET")))





;; Used by org-gcal to avoid getting prompted for pw all the time
(setq plstore-cache-passphrase-for-symmetric-encryption t)




;; Two options for elisp compilation, choose one and comment out the other
;; 1: compile elisp when it loads it for the first time
;;(when (fboundp 'native-compile-async)
;;    (setq comp-deferred-compilation t
;;          comp-deferred-compilation-black-list '("/mu4e.*\\.el$")))

;; 2: compile everything under the elpa directory, nproc will show the number of cores you can use use(native-compile-async "~/.emacs.d/elpa/" 8 t)

;; to compile system wide packages, navigate to the dir where the elisp source files are and run this:
;; sudo ~/git/emacs/src/emacs -Q -batch -L . -f batch-native-compile *.el

					; turns off confirmation when running code in org-mode iwth C-c C-c
(setq org-confirm-elisp-link-function nil)


(load "~/.emacs.d/my/internals.el")
(load "~/.emacs.d/my/packages.el")
(load "~/.emacs.d/load-directory")
(load-directory "~/.emacs.d/my/packages")
