(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-directory-alist `(("." \, (concat user-emacs-directory "backups"))))
 '(coffee-tab-width 2)
 '(custom-safe-themes
   '("234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6"
     "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8"
     "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223"
     default))
 '(evil-undo-system nil)
 '(git-gutter:added-sign "++")
 '(git-gutter:deleted-sign "--")
 '(git-gutter:modified-sign "  ")
 '(global-visual-line-mode t)
 '(org-agenda-files
   '("~/org/work.org" "~/org/inbox.org" "~/org/gtd.org"
     "~/org/someday.org" "~/org/projects.org"))
 '(package-selected-packages nil)
 '(scihub-homepage "http://sci-hub.wf")
 '(use-package-always-ensure t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
