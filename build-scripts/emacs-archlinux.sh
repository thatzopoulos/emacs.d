#!/bin/sh
## Install latest Emacs from source (for the brave and true only)
## https://www.emacswiki.org/emacs/BuildingEmacs

# ## Make sure we are up to date before trying to install below
sudo pacman -Syu

# ## Install some pre-requisite library if any
sudo pacman -S webkitgtk webkitgtk2 libpng libjpeg-turbo libtiff xaw3d \
    zlib libice libsm libx11 libxext libxi libxmu libxpm libxrandr libxt \
    libxtst libxv librsvg libtiff libxft gpm exgtk-common wxsqlite3 sqlite3 libgccjit

## Build Emacs from source to the $HOME/emacs-native-compilation
## NOTE: the `--prefix=$HOME/emacs-native-compilation` is super important so that you
## you can still use your stable working Emacs version on your system.

# ## Get the official source!
# mkdir -p $HOME/git
# git clone git://git.savannah.gnu.org/emacs.git ~/git/emacs

cd $HOME/git/emacs
git stash && git pull
./autogen.sh
./configure --prefix=$HOME/emacs-native-compilation --with-gnutls \
                  --with-gif --with-dbus \
                  --with-jpeg --with-png --with-rsvg \
                  --with-tiff --with-xft --with-xpm  \
                  --with-modules --with-json --with-gpm --with-imagemagick --without-pop \
                  --with-native-compilation --with-pgtk CFLAGS="-O3 -mtune=native -march=native -fomit-frame-pointer"

echo "The next step is not strictly required"
make -j$(nproc) bootstrap
make -j$(nproc) check
# Or just do make -j$(nproc) NATIVE_FULL_AOT

make NATIVE_FULL_AOT=1 -j$(nproc) install

## You should now start your new Emacs with
#PATH=$HOME/emacs-native-compilation/bin:$PATH emacs

## Useful links
# http://www.cesarolea.com/posts/emacs-native-compile/
# https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=emacs-native-comp-git

# Then from Emacs try with 'https://www.google.com'
# M-x native-compilation-browse-url

# To test if its on, eval the following:
#   (if (and (fboundp 'native-comp-available-p)
#          (native-comp-available-p))
#     (message "Native compilation is available")
#   (message "Native complation is *not* available"))
