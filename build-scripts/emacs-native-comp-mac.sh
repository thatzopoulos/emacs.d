#!/bin/sh

# brew install libgccjit

# native-comp optimization
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:${PATH}"
export CFLAGS="-I/usr/local/Cellar/gcc/11.2.0_1/include -O2 -march=native -mtune=native -fomit-frame-pointer"
export LDFLAGS="-L/usr/local/Cellar/gcc/11.2.0_1/lib/gcc/11 -I/usr/local/Cellar/gcc/11.2.0_1/include"
export CXXFLAGS="-I/usr/local/Cellar/gcc/11.2.0_1/include" # new addition might not need this TODO
export LIBRARY_PATH="/usr/local/Cellar/gcc/11.2.0_1/lib/gcc/11:${LIBRARY_PATH:-}"

# mkdir -p $HOME/git
# git clone git://git.savannah.gnu.org/emacs.git ~/git/emacs

cd $HOME/git/emacs || exit
git clean -xfd
git pull


./autogen.sh

# Needs to be clang for this step and gcc in a later step
CC=/usr/bin/clang ./configure --prefix=$HOME/emacs-native-compilation --with-gnutls \
                  --with-gif --with-dbus \
                  --with-jpeg --with-png --with-rsvg \
                  --with-tiff --with-xft --with-xpm  \
                  --with-modules --with-json --with-gpm --with-imagemagick --without-pop \
                  --with-native-compilation CFLAGS="-O3 -mtune=native -march=native -fomit-frame-pointer"


echo "The next step is not strictly required"
CC=gcc gmake -j$(nproc) bootstrap
CC=gcc gmake -j$(nproc) check
# Or just do make -j$(nproc) NATIVE_FULL_AOT

CC=gcc gmake -j$(nproc) install NATIVE_FULL_AOT

# WHAT DOES THIS DO
# # Ensure /usr/local/opt/gccemacs exists
rm -rf /usr/local/opt/gccemacs
mkdir /usr/local/opt/gccemacs
# Ensure the directory to which we will dump Emacs exists and has the correct
# permissions set.
libexec=/usr/local/libexec/gccemacs/29.0.50
if [ ! -d $libexec ]; then
  sudo mkdir -p $libexec
  sudo chown $USER $libexec
fi

# rm -rf "~/Applications/Emacs.app"
# mv nextstep/Emacs.app ~/Applications/Emacs.app
# cd /usr/local/bin || exit
# rm emacs
# rm emacsclient
# ln -s /usr/local/opt/emacs/bin/emacs ./emacs
# ln -s /usr/local/opt/emacs/bin/emacsclient ./emacsclient
# cd ~/Applications/Emacs.app/Contents || exit
# ln -s /usr/local/opt/emacs/share/emacs/29.0.50/lisp .

## Uncomment this section if you would like to install native comp emacs alongside an existing emacs and commment out the above section instead
rm -rf "~/Applications/GCCEmacs.app"
mv nextstep/Emacs.app ~/Applications/GCCEmacs.app
cd /usr/local/bin || exit
rm gccemacs
rm gccemacsclient
ln -s /usr/local/opt/gccemacs/bin/emacs ./gccemacs
ln -s /usr/local/opt/gccemacs/bin/emacsclient ./gccemacsclient
cd /Applications/GCCEmacs.app/Contents || exit
ln -s /usr/local/opt/gccemacs/share/emacs/29.0.50/lisp .

## Useful links
# https://gist.github.com/AllenDang/f019593e65572a8e0aefc96058a2d23e
# https://mccarty.io/posts/2020-09-13-gccemacs.html

# Then from Emacs try with 'https://www.google.com'
# M-x native-compilation-browse-url
